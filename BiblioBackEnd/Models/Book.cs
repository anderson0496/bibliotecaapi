﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiblioBackEnd.Models
{
    public class Book
    {
        public int Id { get; set; }
        public String name { get; set; }
        public int isbn { get; set; }
        public int authorId { get; set; }
        public Author author { get; set; }
        public int categoryId { get; set; }
        public Category category { get; set; }

    }
}
