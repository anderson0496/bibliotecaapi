﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiblioBackEnd.Models
{
    public class Author
    {
        public int Id { get; set; }
        public String name { get; set; }
        public String lastName { get; set; }
        public DateTime birth { get; set; }
        public List<Book> books { get; set; }
    }
}
