﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiblioBackEnd.Models
{
    public class Category
    {
        public int Id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public List<Book> books { get; set; }
    }
}
