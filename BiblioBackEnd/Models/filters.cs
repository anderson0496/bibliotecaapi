﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiblioBackEnd.Models
{
    public class filters
    {
        public string name { get; set; }
        public int authorId { get; set; }
        public int categoryId { get; set; }
    }
}
