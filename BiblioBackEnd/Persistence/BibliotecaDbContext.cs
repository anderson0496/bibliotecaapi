﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiblioBackEnd.Persistence
{
    public class BibliotecaDbContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=biblioteca;Username=biblioteca;Password=biblioteca");
        }

       public DbSet<Models.Book> Books { get; set; }
       public DbSet<Models.Author> Authors { get; set; }
       public DbSet<Models.Category> Categories { get; set; }

    }

}
