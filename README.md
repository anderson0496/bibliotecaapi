# BibliotecaAPI
EL proyecto tiene un motor de base de datos Postgrest

creamos el usuario a usar para la conexion
	* CREATE USER biblioteca WITH PASSWORD 'biblioteca';	
 
nos conectamos a la consola de postgrest y creamos la base de datos
 	* CREATE DATABASE biblioteca OWNER biblioteca;

nos conectamos con la base de datos
    *  \connect biblioteca

Eliminamos el esquema publico para modificarlo
	* DROP SCHEMA public;

Agregamos el esquema publico con el usuario anteriormente creado
	* CREATE SCHEMA public AUTHORIZATION biblioteca;
 	* ALTER SCHEMA public OWNER TO biblioteca;

Dentro del proyecto Encontramos el FrontEnd *BiblioFrontEnd*

tenemos que tener el npm instalado en nuestro equipo para realizar el siguiente paso

dentro de la consola de nuestro sistema nos ubicamos en el proyecto *BiblioFrontEnd*

Ejecutamos 
	* npm install

Luego de esto podremos correr 
	* ng serve

Con esto ya tenemos corriendo nuestro frontEnd en el puerto http://localhost:4200

Para correr la migracion en nuestra consola nos ubicamos en el proyecto BackEnd  *BiblioBackEnd*

y ejecutamos
	* dotnet ef database update

Con esto ya solo nos faltaria correr el servido con el comando
	* dotnet run


