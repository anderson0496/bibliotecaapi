import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import {IAuthor} from '../authors/author';

const  httpOptions = {
      headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin' : '*'
          })
};

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
    public url: string;
    
    constructor(
        public http: HttpClient,
    ){
        this.url = "https://localhost:5001/api/Authors";
    }
    getAuthors(): Observable<any>{
    	return this.http.get(this.url);

    }

    createAuthor(author : IAuthor): Observable<IAuthor>{
    	return this.http.post<IAuthor>(this.url, author,httpOptions);
    }

    getAuthor(authorId: String): Observable<IAuthor>{
        return this.http.get<IAuthor>(this.url + '/' + authorId);

    }

    updateAuthor(author : IAuthor): Observable<IAuthor>{
        return this.http.put<IAuthor>(this.url +"/" +author.id,author,httpOptions);
    }

    deleteAuthor(authorId: string): Observable<IAuthor>{
        return this.http.delete<IAuthor>(this.url + "/" + authorId);
    }
}
