import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import {ICategory} from '../category/category';


const  httpOptions = {
      headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin' : '*'
          })
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
 	public url: string;
  	constructor(
        public http: HttpClient,
    ){
        this.url = "https://localhost:5001/api/Categories";
    }
    getCategories(): Observable<any>{
    	return this.http.get(this.url);

    }

    createCategory(category : ICategory): Observable<ICategory>{
    	return this.http.post<ICategory>(this.url, category,httpOptions);
    }

    getCategory(categoryId: String): Observable<ICategory>{
        return this.http.get<ICategory>(this.url + '/' + categoryId);

    }

    updateCategory(category : ICategory): Observable<ICategory>{
        return this.http.put<ICategory>(this.url +"/" +category.id,category,httpOptions);
    }

    deleteCategory(categoryId: string): Observable<ICategory>{
        return this.http.delete<ICategory>(this.url + "/" + categoryId);
    }
}
