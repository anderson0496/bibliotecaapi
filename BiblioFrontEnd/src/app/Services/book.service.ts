import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import {IBook} from '../book/book';
import {IBookCreate} from '../book/BookCreate';
import {IBookGet} from '../book/bookGet';
import {IFilters} from '../book/filters';


const  httpOptions = {
      headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin' : '*'
          })
};
@Injectable({
  providedIn: 'root'
})
export class BookService {
	public url: string;
  	constructor(
        public http: HttpClient,
    ){
        this.url = "https://localhost:5001/api/Books";
    }
    getBooks(): Observable<any>{
    	return this.http.get(this.url);

    }

    createBook(book : IBookCreate): Observable<IBookCreate>{
    	return this.http.post<IBookCreate>(this.url, book,httpOptions);
    }

    getBook(bookId: String): Observable<IBook>{
        return this.http.get<IBook>(this.url + '/' + bookId);

    }

    updateBook(book : IBookCreate): Observable<IBookCreate>{
        return this.http.put<IBookCreate>(this.url +"/" +book.id,book,httpOptions);
    }

    deleteBook(bookId: string): Observable<IBook>{
        return this.http.delete<IBook>(this.url + "/" + bookId);
    }

    searchBook(filters): Observable<any>{
        return this.http.post<any>(this.url+ "/GetLatestItems",filters,httpOptions);
    }
}
