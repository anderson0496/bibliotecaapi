import { Component, OnInit } from '@angular/core';
import {ICategory} from './category';
import {CategoryService} from '../Services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories : ICategory[];
  constructor( private categoryService : CategoryService) { }

  ngOnInit() {
  	this.loadData();
  }


  deleteCategory(category : ICategory){
      this.categoryService.deleteCategory(category.id.toString())
      .subscribe(category => this.loadData(),
        error => console.log(error))
  }

  loadData(){
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories, 
      error => console.error(error));
  }

}
