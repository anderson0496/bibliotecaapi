import { Component, OnInit } from '@angular/core';
import {ICategory} from '../category';
import { FormBuilder, FormGroup } from '@angular/forms';
import {CategoryService} from 'src/app/Services/category.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {

  constructor(private fb : FormBuilder,
  	private categoryService : CategoryService,
  	private router : Router,
    private activatedRoute : ActivatedRoute) { }

 action : boolean = false;
  categoryId :number;
  formGroup: FormGroup;
  ngOnInit() {
  	this.formGroup = this.fb.group({
  		name: '',
  		description: ''
  	});

    this.activatedRoute.params.subscribe(params =>{
      if(params["id"] == undefined){
        return;
      }
    this.action = true;
    this.categoryId = params["id"];
    this.categoryService.getCategory(this.categoryId.toString())
    .subscribe(category => this.loadForm(category),
      error => this.router.navigate(["/categoryCreate"]));
    })
  }



  save(){
    let category : ICategory= Object.assign({},this.formGroup.value);
    if(this.action){
      category.id = this.categoryId;
      this.categoryService.updateCategory(category)
      .subscribe(category => this.onSaveSuccess()),
      error => console.log(error);

    } else {


    this.categoryService.createCategory(category)
    .subscribe(category => this.onSaveSuccess(),
      error => console.log(error));

    }
  	
  }  

  onSaveSuccess(){
  	this.router.navigate(["/category"]);
  }

  loadForm(category : ICategory){

    this.formGroup.patchValue({
      name: category.name,
      description: category.description

    });
  }

}
