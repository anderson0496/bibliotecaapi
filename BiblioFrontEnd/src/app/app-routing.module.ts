import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorsComponent } from './authors/authors.component';
import { HomeComponent } from './home/home.component';
import { CategoryComponent } from './category/category.component';
import { BookComponent } from './book/book.component';
import {AuthorFormComponent } from './authors/author-form/author-form.component'
import { CategoryFormComponent } from './category/category-form/category-form.component';
import { BookFormComponent } from './book/book-form/book-form.component';



const routes: Routes = [
{path: 'home', component: HomeComponent},
{path: 'author', component: AuthorsComponent},
{path: 'category', component: CategoryComponent},
{path: 'book', component: BookComponent},
{path: 'authorCreate', component: AuthorFormComponent},
{path: 'authorUpdate/:id', component: AuthorFormComponent},
{path: 'categoryCreate', component: CategoryFormComponent},
{path: 'categoryUpdate/:id', component: CategoryFormComponent},
{path: 'bookCreate', component: BookFormComponent},
{path: 'bookUpdate/:id', component: BookFormComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent,AuthorsComponent,CategoryComponent,
								 BookComponent,AuthorFormComponent, CategoryFormComponent,
								 BookFormComponent]
