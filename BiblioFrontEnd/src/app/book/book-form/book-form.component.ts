import { Component, OnInit } from '@angular/core';
import {IBook} from '../book';
import { FormBuilder, FormGroup,FormControl,Validators} from '@angular/forms';
import {BookService} from 'src/app/Services/book.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {CategoryService} from 'src/app/Services/category.service';
import {ICategory} from 'src/app/category/category';
import {IAuthor} from 'src/app/authors/author';
import { CompleterService, CompleterData } from 'ng2-completer';
import {AuthorService} from 'src/app/Services/author.service';
import {IBookCreate} from '../BookCreate';
import {IBookGet} from '../bookGet';


@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  action : boolean = false;
  selectedLevel: number = 0;
  bookId :number;
  categories : ICategory[];
  category : ICategory;
  formGroup: FormGroup;
  keyword = 'name';
  authors : IAuthor[];
  constructor(private fb : FormBuilder,
  	private bookService : BookService,
  	private router : Router,
    private activatedRoute : ActivatedRoute,
    private categoryService : CategoryService,
    private authorService : AuthorService,) { }



  ngOnInit() {
    this.loadDataCategories();
    this.loadDataAuthors();
  	this.formGroup = this.fb.group({
	  name : '',
	  isbn : '',
	  Category : new FormControl(),
    Author : ['',Validators.required]
  	});

    this.activatedRoute.params.subscribe(params =>{
      if(params["id"] == undefined){
        return;
      }
    this.action = true;
    this.bookId = params["id"];
    this.bookService.getBook(this.bookId.toString())
    .subscribe(book =>this.loadForm(book),
      error => this.router.navigate(["/bookCreate"]));
    })
  }

  loadDataCategories(){
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories, 
      error => console.error(error));
  }

  loadDataAuthors(){
    this.authorService.getAuthors().subscribe(
      authors => this.authors = authors, 
      error => console.error(error));
  }



  onChangeLevel(LevelId: number) {

   this.category = this.categories.find(function(element) {
    return element.id == LevelId;
    });
  }

  save(){
    let book : IBook= Object.assign({},this.formGroup.value);
        book.categoryId = this.category.id;
    book.authorId = book.Author.id;
    let bookTrans={
      id : null,
      name : book.name,
      isbn : book.isbn,
      authorId : book.authorId,
      categoryId : book.categoryId
    }

    let bookCreate : IBookCreate=Object.assign({}, bookTrans);
    if(this.action){
      bookCreate.id = this.bookId;
      console.log(bookCreate);
      this.bookService.updateBook(bookCreate)
      .subscribe(book => this.onSaveSuccess()),
      error => console.log(error);

    } else {
      bookCreate.id = null;
    this.bookService.createBook(bookCreate)
    .subscribe(book => this.onSaveSuccess(),
      error => console.log(error));

    }
  	
  }  

  onSaveSuccess(){
  	this.router.navigate(["/book"]);
  }

  loadForm(book : IBook){

  this.formGroup.patchValue({
  name: book.name,
  isbn : book.isbn
  });
 
  }


}
