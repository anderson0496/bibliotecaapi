import {IAuthor} from 'src/app/authors/author';
import {ICategory} from 'src/app/category/category';
export interface IBookGet {
	id : number;
	name : string;
	isbn : number;
	author : IAuthor;
	category: ICategory;
}