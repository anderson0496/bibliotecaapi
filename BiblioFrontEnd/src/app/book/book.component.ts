import { Component, OnInit } from '@angular/core';
import {IBook} from './book';
import {IFilters} from './filters';
import { CompleterService, CompleterData } from 'ng2-completer';
import { FormBuilder, FormGroup,FormControl,Validators} from '@angular/forms';
import {AuthorService} from 'src/app/Services/author.service';
import {BookService} from '../Services/book.service';
import {CategoryService} from 'src/app/Services/category.service';
import {ICategory} from 'src/app/category/category';
import {IAuthor} from 'src/app/authors/author';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  keyword = 'name';
  selectedLevel: number = 0;
  books : IBook[];
  filter : IFilters;
  categories : ICategory[];
  category : ICategory;
  authors : IAuthor[];
  formGroup: FormGroup;
  constructor( private bookService : BookService,
        private categoryService : CategoryService,
        private authorService : AuthorService,
        private fb : FormBuilder,) { }

  ngOnInit() {

    this.formGroup = this.fb.group({
    name : null,
    categoryId : null,
    Author : null
    });

    this.loadDataCategories();
    this.loadDataAuthors();
  	this.loadData();
  }

   onChangeLevel(LevelId: number) {

   this.category = this.categories.find(function(element) {
    return element.id == LevelId;
    });
  }
  deleteBook(book : IBook){
      this.bookService.deleteBook(book.id.toString())
      .subscribe(book => this.loadData(),
        error => console.log(error))
  }

  loadData(){
    this.bookService.getBooks().subscribe(
      books => this.books = books, 
      error => console.error(error));
  }

   loadDataCategories(){
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories, 
      error => console.error(error));
  }

  loadDataAuthors(){
    this.authorService.getAuthors().subscribe(
      authors => this.authors = authors, 
      error => console.error(error));
  }

  search(){
    let filter : IFilters= Object.assign({},this.formGroup.value);

    let fil = {
      name: '',
      authorId: 0,
      categoryId: 0
    }
    if(filter.name != null){
      fil.name = filter.name;
    }
    if(filter.Author != null){
      fil.authorId = filter.Author.id;
    }
    if(filter.categoryId != null){
      fil.categoryId = filter.categoryId;
    }

    this.bookService.searchBook(fil).subscribe(
      books => this.books = books,  
      error => console.error(error));
  }

  clearFilter(){
    this.formGroup = this.fb.group({
    name : null,
    categoryId : null,
    Author : null
    });
  }

}
