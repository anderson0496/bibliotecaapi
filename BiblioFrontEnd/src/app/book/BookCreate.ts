export interface IBookCreate {
	id : number;
	name : string;
	isbn : number;
	categoryId : number;
	authorId: number;
}