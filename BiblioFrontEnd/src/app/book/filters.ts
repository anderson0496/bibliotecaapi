import {IAuthor} from 'src/app/authors/author';
export interface IFilters {

	name : string;
	authorId : number;
	categoryId : number;
	Author : IAuthor;
}