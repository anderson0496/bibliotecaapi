import {IAuthor} from 'src/app/authors/author';
import {ICategory} from 'src/app/category/category';
export interface IBook {
	id : number;
	name : string;
	isbn : number;
	Author : IAuthor;
	Category: ICategory;
	categoryId : number;
	authorId: number;
}