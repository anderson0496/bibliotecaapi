import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import {HttpClientModule} from '@angular/common/http';
import { BookComponent } from './book/book.component';
import {AuthorService} from './Services/author.service';
import { AuthorFormComponent } from './authors/author-form/author-form.component';
import { CategoryFormComponent } from './category/category-form/category-form.component';
import { BookFormComponent } from './book/book-form/book-form.component';
import { Ng2CompleterModule } from "ng2-completer";
import {AutocompleteLibModule} from 'angular-ng-autocomplete';



@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    NavMenuComponent,
    BookComponent,
    AuthorFormComponent,
    CategoryFormComponent,
    BookFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    Ng2CompleterModule,
    AutocompleteLibModule,
  ],
  providers: [AuthorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
