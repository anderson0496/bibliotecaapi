import { Component, OnInit } from '@angular/core';
import {IAuthor} from '../author';
import { FormBuilder, FormGroup } from '@angular/forms';
import {AuthorService} from 'src/app/Services/author.service';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {DatePipe } from '@angular/common';

Router
@Component({
  selector: 'app-author-form',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.css']
})
export class AuthorFormComponent implements OnInit {

  constructor( private fb : FormBuilder,
  	private authorService : AuthorService,
  	private router : Router,
    private activatedRoute : ActivatedRoute) { }


  action : boolean = false;
  authorId :number;
  formGroup: FormGroup;
  ngOnInit() {
  	this.formGroup = this.fb.group({
  		name: '',
  		lastName: '',
  		birth: ''
  	});

    this.activatedRoute.params.subscribe(params =>{
      if(params["id"] == undefined){
        return;
      }
    this.action = true;
    this.authorId = params["id"];
    this.authorService.getAuthor(this.authorId.toString())
    .subscribe(author => this.loadForm(author),
      error => this.router.navigate(["/authorCreate"]));
    })
  }



  save(){
    let author : IAuthor= Object.assign({},this.formGroup.value);
    if(this.action){
      author.id = this.authorId;
      this.authorService.updateAuthor(author)
      .subscribe(author => this.onSaveSuccess()),
      error => console.log(error);

    } else {


    this.authorService.createAuthor(author)
    .subscribe(author => this.onSaveSuccess(),
      error => console.log(error));

    }
  	
  }  

  onSaveSuccess(){
  	this.router.navigate(["/author"]);
  }

  loadForm(author : IAuthor){

   
    var format = "yyyy-MM-dd";
    this.formGroup.patchValue({
      name: author.name,
      lastName: author.lastName,
      birth: author.birth
    });
  }

}
