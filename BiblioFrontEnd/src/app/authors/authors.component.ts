import { Component, OnInit } from '@angular/core';
import {IAuthor} from './author';
import {AuthorService} from '../Services/author.service';



@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  authors : IAuthor[];
  constructor( private authorService : AuthorService) { }

  ngOnInit() {
  	this.loadData();
  }


  deleteAuthor(author : IAuthor){
      this.authorService.deleteAuthor(author.id.toString())
      .subscribe(author => this.loadData(),
        error => console.log(error))
  }

  loadData(){
    this.authorService.getAuthors().subscribe(
      authors => this.authors = authors, 
      error => console.error(error));
  }

}
